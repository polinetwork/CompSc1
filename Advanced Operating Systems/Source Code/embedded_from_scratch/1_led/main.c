
#include "registers.h"

//PD14: red led

void delay()
{
	volatile int i;
	int j=0;
	for(i=0;i<1000000;i++)
	{
		j++;
	}
}

int main()
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
	
	for(int n = 0;;n++)
	{
		GPIOD->MODER |= 1<<24+n*2;
		GPIOD->BSRR=1<<15;
		delay();
		GPIOD->BSRR=1<<(15+16);
		delay();
	}
}
