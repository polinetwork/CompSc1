create trigger addFile
after insert on File
for each row

declare maxspace integer
declare driveid integer

begin 

	select max(FreeBlocks) into maxspace 
	from DiskMapping
	where BucketId = hf(new.Filename)

	if (maxspace > new.RequiredBlocks)
	then 

		(select Driveid into driveid
		from DiskMapping
		where BucketId = hf(new.Filename)
		and FreeBlocks = max(FreeBlocks))

	else

		begin

			select min(DriveId) into driveid from Disk D where D.Driveid not in
			(select DriveId from DiskMapping)

			insert into DiskMapping values(
			driveid,
			hf(new.Filename),
			(select TotalBlocks from Disk where Driveid = driveid)

		end


	insert into FileMapping values(new.Filename, driveid)

	update DiskMapping
	set FreeBlocks = FreeBlocks - new.RequiredBlocks
	where Driveid = driveid)

end


create trigger removeFile
after delete on File
for each row

declare driveid integer

begin 

(select Driveid into driveid 
from DiskMapping where BucketId = hf(old.Filename))

delete from FileMapping where Filename = old.Filename

update DiskMapping DM
set DM.FreeBlocks = DM.FreeBlocks - TotalBlocks
where Driveid = driveid

if ( 
	select FreeBlocks from DiskMapping where Driveid = driveid
	= 
	select TotalBlocks from Disk where DriveId = driveid
)
then
delete from DiskMapping where Driveid = driveid
else 
PRINT('SQL SUCKS')








