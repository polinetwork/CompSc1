%Moments of inertia of the motors
Jm1=5.e-3;
Jm2=2.e-3;

%Stiffness constants of the transmissions
Kel1=70;
Kel2=70;
Kel=diag([Kel1,Kel2]);

%Viscous friction coefficients of the transmissions
Del1=0.05;
Del2=0.05;

%Reduction ratios
n1=100;
n2=100;
N=diag([n1,n2]);

%Robot definitions

L1=link([0,1,0,0,0]);
L1.m=50;
L1.r=[-0.5,0,0];
L1.I=[0,0,0;0,0,0;0,0,10];
L1.G=1;
L1.Jm=0;
%Note:  motors and transmissions are defined outside the block which simulates the robot

L2=L1;

r2=robot({L1,L2});

r2.name='POLIrobot';
r2.gravity=[0,9.81,0];

%Instant of brake release instant
ton=0.1;

%Initial instant of motion
t0=0.5;

%Travel time
T=0.5;

%Maximum speed
sd=5;

%Distance to cover
h=1.6;

%Acceleration time
ta=(T*sd-h)/sd;

%Maximum acceleration
amax=sd/ta;

%Initial value for the trajectory
s0=0.2;

%Initial reference position at the joints
qrif0=ikine(r2,[eye(3),[0.2;0;0];[0,0,0,1]],[-1,3],[1 1 0 0 0 0 ]);

%Initial motor positions
qm0=qrif0*N;

%Initial link positions
q0=fsolve(@(q0) N*Kel*(qm0'-N*q0')-gravload(r2,q0)',qrif0);

%Load side torques at steady state
tau0=gravload(r2,q0);

%Motor side torques at steady state
taum0=tau0/N;


%Load side reference values of the moments of inertia
Jl1=60;
Jl2=22.5;

%Same values referred to the motor axes
Jlr1=Jl1/n1^2;
Jlr2=Jl2/n2^2;

%Inertia ratios
ro1=Jlr1/Jm1;
ro2=Jlr2/Jm2;

%Parameters: axis 1

mu1=1/(Jm1+Jlr1);
wz1=sqrt(Kel1/Jlr1);
csiz1=0.5*Del1/sqrt(Jlr1*Kel1);
wp1=sqrt(1+ro1)*wz1;
csip1=sqrt(1+ro1)*csiz1;
Gvm1=tf(mu1*[1/wz1^2 2*csiz1/wz1 1],[1/wp1^2 2*csip1/wp1 1 0]);

%Tuning: axis 1 controller

Tiv1=
Kiv1= 
Kpv1=
wcv1=
wtildecv1=

Rv1 =
Lv1=
Fv1=
Gpm1 = 
Kpp1=

%Parameters: axis 2

mu2=
wz2=
csiz2=
wp2=
csip2=
Gvm2=

%Tuning: axis 2 controller

Tiv2=
Kiv2=
Kpv2=
wcv2=
wtildecv2=

Rv2 =
Lv2=
Fv2=
Gpm2 = 
Kpp2=
