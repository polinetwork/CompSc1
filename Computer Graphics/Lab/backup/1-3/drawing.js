function main(){
	var canvas = document.getElementById("my-canvas");
	var context= canvas.getContext("2d");
	
	// Grid
	
	for(var x = 0.5; x < 800; x +=10){
		context.moveTo(x,0);	
		context.lineTo(x,600);
	}
	
	for(var y = 0.5; y < 600; y +=10){
		context.moveTo(0, y);	
		context.lineTo(800, y);
	}
	
	context.strokeStyle = "#eee";
	context.stroke();
	
	//X-Axis
	
	context.beginPath();
	context.moveTo(0,40);
	context.lineTo(240, 40);
	context.moveTo(260, 40);
	context.lineTo(800, 40);	
	context.moveTo(795, 35);
	context.lineTo(800, 40);
	context.lineTo(795, 45);
	
	context.moveTo(60, 0);
	context.lineTo(60, 153);
	context.moveTo(60, 173);
	context.lineTo(60, 600);	
	context.moveTo(65, 595);
	context.lineTo(60, 600);
	context.lineTo(55, 595);

	context.strokeStyle = "#000";
	context.stroke();
	
	context.font = "bold 12px sans-serif";
	context.fillText("x", 248, 43);
	context.fillText("y", 58, 165);
	
	context.Baseline = "top";
	context.fillText("(0,0)", 8, 15);
	
	context.textAlign = "right";
	context.Baseline = "bottom";
	context.fillText("(800,600)", 792, 590);
	
	context.fillRect(0,0,3,3);
	context.fillRect(797,597,3,3);
	
	// circle
	
	var segments = 64;
	var radius = 100;
	var cx = 400;
	var cy = 300;
	
	context.moveTo(cx + radius, cy);
	
	for (var i = 0; i <= segments; i ++){
		var alpha = (i / segments) * 2 * Math.PI;
		context.lineTo(cx + radius * Math.cos(alpha), cy + radius * Math.sin(alpha));
	}
	context.lineWidth=1;
	context.strokeStyle = "#000";
	context.stroke();	
}