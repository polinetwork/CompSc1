var canvas;
var gl = null,
	program = null;
	
var projectionMatrix, 
	perspectiveMatrix,
	viewMatrix;

var vertexPositionHandle, matrixPositionHandle;

//Parameters for Camera
var cx = 0.5;
var cy = 0.0;
var cz = 1.0;
var elevation = 0.0;
var angle = -30.0;

var delta = 0.1;

function main(){

	canvas=document.getElementById("my-canvas");
	try{
		gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
	}catch(e){
		 console.log(e);
	}
	if(gl){
		
		//Setting the size for the canvas equal to half the browser window
		var w=canvas.clientWidth;
		var h=canvas.clientHeight;
		
		gl.clearColor(0.0, 0.0, 0.0, 1.0);
		gl.viewport(0.0, 0.0, w, h);
		
		perspectiveMatrix = utils.MakePerspective(90, w/h, 0.1, 100.0);
		console.log(perspectiveMatrix);

		
		var VBO = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, VBO);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
		
		//*** The (for now) obscure GLSL part... to take for granted.
			var vs = 'attribute vec3 pos1; uniform mat4 pMatrix; void main() { gl_Position = pMatrix*vec4(pos1, 1.0);}';
			var fs = 'precision mediump float; void main() { gl_FragColor = vec4(0.8,0,0,1); }';
			program = gl.createProgram();
			var v1 = gl.createShader(gl.VERTEX_SHADER);
			gl.shaderSource(v1, vs);
			gl.compileShader(v1);
			if (!gl.getShaderParameter(v1, gl.COMPILE_STATUS)) {
				alert("ERROR IN VS SHADER : " + gl.getShaderInfoLog(v1));
			}
			var v2 = gl.createShader(gl.FRAGMENT_SHADER);
			gl.shaderSource(v2, fs)
			gl.compileShader(v2);	
			if (!gl.getShaderParameter(v2, gl.COMPILE_STATUS)) {
				alert("ERROR IN VS SHADER : " + gl.getShaderInfoLog(v2));
			}			
			gl.attachShader(program, v1);
			gl.attachShader(program, v2);
			gl.linkProgram(program);		
		//*** End of the obscure part
		
		gl.useProgram(program);
		
		vertexPositionHandle = gl.getAttribLocation(program, 'pos1');
		matrixPositionHandle = gl.getUniformLocation(program, 'pMatrix');
		
		console.log(vertexPositionHandle + "  " + matrixPositionHandle);
		
		//Setting up the interaction using keys
		initInteraction();
		
		//Rendering cycle
		drawScene();

		
	}else{
		alert( "Error: Your browser does not appear to support WebGL.");
	}
	
}

function initInteraction(){
		var keyFunction =function(e) {
			
			if (e.keyCode == 37) {	// Left arrow
				cx-=delta;
			}
			if (e.keyCode == 39) {	// Right arrow
				cx+=delta;
			}	
			if (e.keyCode == 38) {	// Up arrow
				cz-=delta;
			}
			if (e.keyCode == 40) {	// Down arrow
				cz+=delta;
			}
			if (e.keyCode == 107) {	// Add
				cy+=delta;
			}
			if (e.keyCode == 109) {	// Subtract
				cy-=delta;
			}
			
			if (e.keyCode == 65) {	// a
				angle-=delta*10.0;
			}
			if (e.keyCode == 68) {	// d
				angle+=delta*10.0;
			}	
			if (e.keyCode == 87) {	// w
				elevation+=delta*10.0;
			}
			if (e.keyCode == 83) {	// s
				elevation-=delta*10.0;
			}
			
		}
		//'window' is a JavaScript object (if "canvas", it will not work)
		window.addEventListener("keyup", keyFunction, false);
	}


function computeMatrices(){
		viewMatrix = utils.MakeView(cx, cy, cz, elevation, angle);
		projectionMatrix = utils.multiplyMatrices(perspectiveMatrix, viewMatrix);
}
	
function drawScene(){

		computeMatrices();

		gl.uniformMatrix4fv(matrixPositionHandle, gl.FALSE, utils.transposeMatrix(projectionMatrix));
		
		gl.enableVertexAttribArray(vertexPositionHandle);
		gl.vertexAttribPointer(vertexPositionHandle, 3, gl.FLOAT, false, 3 * 4, 0);
		
		gl.drawArrays(gl.LINES, 0, 24);
		
		window.requestAnimationFrame(drawScene);
}



